FROM python:3.7-slim AS build

WORKDIR /app
COPY . /app

RUN pip3 install .[prod]

FROM python:3.7-slim

COPY --from=build /usr/local/bin/gunicorn /usr/local/bin/gunicorn
COPY --from=build /usr/local/lib/python3.7/site-packages/ /usr/local/lib/python3.7/site-packages/

USER 65081
EXPOSE 8000/tcp
CMD ["gunicorn", "--bind=0.0.0.0:8000", "--access-logfile=-", "tuton_controller:app"]
