# SPDX-License-Identifier: GPL-3.0-or-later

import setuptools

setuptools.setup(
    name='tuton_controller',
    version='0',
    setup_requires=[
        'pytest-runner',
    ],
    install_requires=[
        'flask',
        'marshmallow>=3.0.0b14',
    ],
    extras_require={
        'prod': [
            'gunicorn',
        ],
        'test': [
            'pytest',
        ],
    },
    packages=setuptools.find_namespace_packages(where="src"),
    package_dir={"": "src"},
    package_data={
        '': ['templates/*'],
    },
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)"
    ],
)
