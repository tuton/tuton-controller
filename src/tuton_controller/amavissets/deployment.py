# SPDX-License-Identifier: GPL-3.0-or-later


from ..base.deployment import Deployment as BaseDeployment


class Deployment(BaseDeployment):
    @property
    def spec_template_spec_containers(self):
        ret = []
        if 'amavis' in self.parent.containers:
            ret.append(self.spec_template_spec_containers_amavis(self.parent.containers['amavis']))
        if 'rsyslog' in self.parent.containers:
            ret.append(self.spec_template_spec_containers_rsyslog(self.parent.containers['rsyslog']))
        return ret

    def spec_template_spec_containers_amavis(self, c):
        return {
            'name': 'amavis',
            'image': c.image,
            'imagePullPolicy': c.image_pull_policy,
            'securityContext': {
                'allowPrivilegeEscalation': False,
                'readOnlyRootFilesystem': True,
            },
            'volumeMounts': [
                {
                    'name': 'amavis-config',
                    'mountPath': '/etc/amavis/conf.d/01-base',
                    'subPath': 'amavisd.conf',
                },
                {
                    'name': 'amavis-data',
                    'mountPath': '/var/lib/amavis',
                },
                {
                    'name': 'amavis-tmp',
                    'mountPath': '/var/lib/amavis/tmp',
                },
                {
                    'name': 'amavis-run',
                    'mountPath': '/run/amavis',
                },
                {
                    'name': 'rsyslog-run',
                    'mountPath': '/run/rsyslog',
                },
                {
                    'name': 'spamassassin-config',
                    'mountPath': '/etc/spamassassin/k8s.cf',
                    'subPath': 'spamassassin.conf',
                },
            ]
        }

    def spec_template_spec_containers_rsyslog(self, c):
        return {
            'name': 'rsyslog',
            'image': c.image,
            'imagePullPolicy': c.image_pull_policy,
            'securityContext': {
                'allowPrivilegeEscalation': False,
                'readOnlyRootFilesystem': True,
            },
            'volumeMounts': [
                {
                    'name': 'rsyslog-run',
                    'mountPath': '/run/rsyslog',
                },
            ],
        }

    @property
    def spec_template_spec_volumes(self):
        return [
            {
                'name': 'amavis-config',
                'configMap': {'name': self.parent.metadata.name},
            },
            {
                'name': 'amavis-data',
                'emptyDir': {},
            },
            {
                'name': 'amavis-run',
                'emptyDir': {'medium': 'Memory'},
            },
            {
                'name': 'amavis-tmp',
                'emptyDir': {'medium': 'Memory'},
            },
            {
                'name': 'rsyslog-run',
                'emptyDir': {'medium': 'Memory'},
            },
            {
                'name': 'spamassassin-config',
                'configMap': {'name': self.parent.metadata.name},
            },
        ]
