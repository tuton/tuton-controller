# SPDX-License-Identifier: GPL-3.0-or-later

from flask import render_template

from .. import app
from ..api.te.amavisconfig import AmavisConfig


def test_amavis():
    config = AmavisConfig(metadata=None)
    with app.test_request_context('/amavisset/'):
        render_template('config.pl.j2', config_global=config, config_policybanks={'test': config})


def test_spamassassin():
    config = AmavisConfig(metadata=None)
    with app.test_request_context('/amavisset/'):
        render_template('spamassassin.cf.j2', config_global=config)
