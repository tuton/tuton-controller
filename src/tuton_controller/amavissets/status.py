# SPDX-License-Identifier: GPL-3.0-or-later


class Status:
    def __init__(self, parent, children):
        self.parent = parent
        self.children = children

    def __call__(self):
        deployment = self.children.get('Deployment.apps/v1', {}).get(self.parent.metadata.name, {})

        return {
            'replicas': deployment.get('status', {}).get('replicas', None),
        }
