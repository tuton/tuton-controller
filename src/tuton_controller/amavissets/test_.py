from . import customize, sync
from .. import app


class TestCustomize:
    basic_v1alpha1_AmavisSet = {
        'apiVersion': 'tuton.email/v1alpha1',
        'kind': 'AmavisSet',
        'metadata': {
            'name': 'test',
        },
        'spec': {
        },
    }

    def test_basic_v1alpha1(self):
        data = {
            'parent': self.basic_v1alpha1_AmavisSet,
        }
        with app.test_request_context('/amavisset/', json=data):
            customize()


class TestSync:
    basic_v1alpha1_AmavisSet = {
        'apiVersion': 'tuton.email/v1alpha1',
        'kind': 'AmavisSet',
        'metadata': {
            'name': 'test',
        },
        'spec': {
        },
    }

    basic_v1alpha1_AmavisConfig = {
        'apiVersion': 'tuton.email/v1alpha1',
        'kind': 'AmavisConfig',
        'metadata': {
            'name': 'test',
        },
    }

    def test_basic_v1alpha1(self):
        data = {
            'parent': self.basic_v1alpha1_AmavisSet,
            'children': {},
            'related': {},
        }
        with app.test_request_context('/amavisset/', json=data):
            sync()

    def test_related_v1alpha1(self):
        data = {
            'parent': self.basic_v1alpha1_AmavisSet,
            'children': {},
            'related': {
                'AmavisConfig.tuton.email/v1alpha1': {
                    'test': self.basic_v1alpha1_AmavisConfig,
                },
            },
        }
        with app.test_request_context('/amavisset/', json=data):
            sync()
