# SPDX-License-Identifier: GPL-3.0-or-later

from ..base.service import Service as BaseService


class Service(BaseService):
    spec_ports = [
        {
            'port': 10025,
            'targetPort': 10025,
        },
    ]
