# SPDX-License-Identifier: GPL-3.0-or-later

from flask import Blueprint, jsonify, request
from marshmallow import EXCLUDE

from ..api.te.amavisset import v1alpha1_AmavisSetSchema
from .configmap import ConfigMap
from .deployment import Deployment
from .service import Service
from .status import Status


controller = Blueprint('amavissets', __name__, template_folder='templates', url_prefix='/amavissets')


@controller.route('/customize', methods=('POST', ))
def customize():
    data = request.get_json()
    config_selector = data['parent'].get('spec', {}).get('configSelector', {})
    related = [
        {
            'apiVersion': 'tuton.email/v1alpha1',
            'resource': 'amavisconfigs',
            'labelSelector': config_selector,
        },
    ]
    return jsonify({'relatedResources': related})


@controller.route('/sync', methods=('POST', ))
def sync():
    data = request.get_json()
    parent = v1alpha1_AmavisSetSchema(unknown=EXCLUDE).load(data['parent'])
    children = data['children']
    related = data['related']
    status = Status(parent, children)
    configmap = ConfigMap(parent, related)
    deployment = Deployment(parent, configmap)
    service = Service(parent)
    return jsonify({
        'status': status(),
        'children': [configmap(), deployment(), service()],
    })
