# SPDX-License-Identifier: GPL-3.0-or-later

import hashlib

from flask import render_template

from ..api.te.amavisconfig import AmavisConfig, v1alpha1_AmavisConfigSchema
from ..base.configmap import ConfigMap as BaseConfigMap


class ConfigMap(BaseConfigMap):
    schema = v1alpha1_AmavisConfigSchema()

    base_config = schema.load({
        'apiVersion': 'tuton.email/v1alpha1',
        'kind': 'AmavisConfig',
        'metadata': {
            'name': '__base',
        },
        'spec': {
            'options': [
                {
                    'name': 'myhostname',
                    'value': 'localhost',
                },
                {
                    'name': 'listen_sockets',
                    'value': [':10025'],
                },
                {
                    'name': 'inet_acl',
                    'value': ['0.0.0.0/0', '[::]/0'],
                },
                {
                    'name': 'do_syslog',
                    'value': 1,
                },
                {
                    'name': 'syslog_facility',
                    'value': 'mail',
                },
                {
                    'name': 'logline_maxlen',
                    'value': 8000,
                },
            ],
            'spamassassinOptions': [
                {
                    'name': 'use_learner',
                    'value': '0',
                },
            ],
        }
    })

    def __init__(self, parent, related, load_base_config=True):
        super().__init__(parent)
        self.config = []
        if load_base_config:
            self.config.append(self.base_config)
        for n in ('AmavisConfig.tuton.email/v1alpha1', ):
            self.config.extend(self.schema.load(j) for i, j in sorted(related.get(n, {}).items()))

    @property
    def checksum(self):
        m = hashlib.sha256()
        for i, j in sorted(self.data.items()):
            m.update(i.encode('utf-8'))
            m.update(j.encode('utf-8'))
        return m.hexdigest()

    @property
    def data(self):
        config = self.merge_config()
        return {
            'amavisd.conf': render_template('config.pl.j2', **config),
            'spamassassin.conf': render_template('spamassassin.cf.j2', **config),
        }

    def merge_config(self):
        config_global = AmavisConfig(metadata=self.parent.metadata)
        config_policybanks = {}

        for config in self.config:
            policybank_name = config.policybank_name
            if policybank_name is None:
                config_global.update(config)
            else:
                config_policybanks.setdefault(policybank_name, AmavisConfig()).update(config)

        return {
            'config_global': config_global,
            'config_policybanks': config_policybanks,
        }
