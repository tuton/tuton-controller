# SPDX-License-Identifier: GPL-3.0-or-later


class Service:
    api_version = 'v1'
    kind = 'Service'

    def __init__(self, parent):
        self.parent = parent

    def __call__(self):
        return {
            'apiVersion': self.api_version,
            'kind': self.kind,
            'metadata': self.metadata,
            'spec': self.spec,
        }

    @property
    def metadata(self):
        return {
            'name': self.parent.metadata.name,
        }

    @property
    def spec(self):
        return {
            'ports': self.spec_ports,
            'selector': self.spec_selector,
        }

    @property
    def spec_selector(self):
        return {
            'controller-uid': self.parent.metadata.uid,
        }
