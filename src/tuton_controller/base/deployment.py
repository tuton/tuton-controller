# SPDX-License-Identifier: GPL-3.0-or-later


class Deployment:
    api_version = 'apps/v1'
    kind = 'Deployment'

    def __init__(self, parent, config):
        self.parent = parent
        self.config = config

    def __call__(self):
        return {
            'apiVersion': self.api_version,
            'kind': self.kind,
            'metadata': self.metadata,
            'spec': self.spec,
        }

    @property
    def metadata(self):
        return {
            'name': self.parent.metadata.name,
        }

    @property
    def spec(self):
        return {
            'replicas': self.spec_replicas,
            'selector': self.spec_selector,
            'template': self.spec_template,
        }

    @property
    def spec_replicas(self):
        return self.parent.replicas

    @property
    def spec_selector(self):
        return {
            'matchLabels': {
                'controller-uid': self.parent.metadata.uid,
            },
        }

    @property
    def spec_template(self):
        return {
            'metadata': self.spec_template_metadata,
            'spec': self.spec_template_spec,
        }

    @property
    def spec_template_metadata(self):
        return {
            'name': self.parent.metadata.name,
            'labels': self.spec_template_metadata_labels,
            'annotations': {
                'controller.tuton.email/config-checksum': self.config.checksum,
            },
        }

    @property
    def spec_template_metadata_labels(self):
        labels = self.parent.template_metadata.labels.copy()
        labels['controller-uid'] = self.parent.metadata.uid
        return labels

    @property
    def spec_template_spec(self):
        return {
            'containers': self.spec_template_spec_containers,
            'volumes': self.spec_template_spec_volumes,
        }

    @property
    def spec_template_spec_containers(self):
        raise NotImplementedError

    @property
    def spec_template_spec_volumes(self):
        raise NotImplementedError
