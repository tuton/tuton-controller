# SPDX-License-Identifier: GPL-3.0-or-later


class ConfigMap:
    api_version = 'v1'
    kind = 'ConfigMap'

    def __init__(self, parent):
        self.parent = parent

    def __call__(self):
        return {
            'apiVersion': self.api_version,
            'kind': self.kind,
            'metadata': self.metadata,
            'data': self.data,
        }

    @property
    def data(self):
        raise NotImplementedError

    @property
    def metadata(self):
        return {
            'name': self.parent.metadata.name,
        }
