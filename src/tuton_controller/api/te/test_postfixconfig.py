# SPDX-License-Identifier: GPL-3.0-or-later

from tuton_controller.api.te.postfixconfig import v1alpha1_PostfixConfigSchema


class Test_v1alpha1_PostfixConfigSchema:
    schema = v1alpha1_PostfixConfigSchema()

    def test(self):
        data = {
            'apiVersion': 'tuton.email/v1alpha1',
            'kind': 'PostfixConfig',
            'metadata': {
                'name': 'name',
                'labels': {},
            },
            'spec': {
                'data': {
                    'test': 'test',
                },
                'options': [
                    {
                        'name': 'value',
                        'value': 'value',
                    },
                    {
                        'name': 'secret',
                        'secret': {
                            'secretName': 'name',
                            'subPath': 'path',
                        }
                    },
                ],
                'services': [
                    {
                        'name': 'value',
                        'type': 'unix',
                        'private': True,
                        'unpriv': True,
                        'command': 'command',
                        'options': [
                            {
                                'name': 'value',
                                'value': 'value',
                            },
                        ],
                    },
                ],
            },
        }

        obj = self.schema.load(data)

        assert len(obj.data) == 1
        assert obj.data['test'] == 'test'

        assert len(obj.options) == 2
        assert obj.options['value'].value == 'value'
        assert obj.options['secret'].secret_name == 'name'
        assert obj.options['secret'].sub_path == 'path'

        assert len(obj.services) == 1
        assert obj.services['value', 'unix'].command == 'command'

        assert data == self.schema.dump(obj)

    def test_option_duplicate(self):
        data = {
            'apiVersion': 'tuton.email/v1alpha1',
            'kind': 'PostfixConfig',
            'metadata': {
                'name': 'name',
            },
            'spec': {
                'options': [
                    {
                        'name': 'value',
                        'value': 'value1',
                    },
                    {
                        'name': 'value',
                        'value': 'value2',
                    },
                ],
            },
        }

        obj = self.schema.load(data)

        assert len(obj.options) == 1
        assert obj.options['value'].value == 'value2'

    def test_service_duplicate(self):
        data = {
            'apiVersion': 'tuton.email/v1alpha1',
            'kind': 'PostfixConfig',
            'metadata': {
                'name': 'name',
            },
            'spec': {
                'services': [
                    {
                        'name': 'value',
                        'type': 'unix',
                        'private': True,
                        'unpriv': True,
                        'command': 'command1',
                    },
                    {
                        'name': 'value',
                        'type': 'unix',
                        'private': True,
                        'unpriv': True,
                        'command': 'command2',
                    },
                ],
            },
        }

        obj = self.schema.load(data)

        assert len(obj.services) == 1
        assert obj.services['value', 'unix'].command == 'command2'

    def test_service_same_name(self):
        data = {
            'apiVersion': 'tuton.email/v1alpha1',
            'kind': 'PostfixConfig',
            'metadata': {
                'name': 'name',
            },
            'spec': {
                'services': [
                    {
                        'name': 'value',
                        'type': 'inet',
                        'private': False,
                        'unpriv': True,
                        'command': 'command1',
                    },
                    {
                        'name': 'value',
                        'type': 'pass',
                        'private': True,
                        'unpriv': True,
                        'command': 'command2',
                    },
                    {
                        'name': 'value',
                        'type': 'unix',
                        'private': True,
                        'unpriv': True,
                        'command': 'command3',
                    },
                ],
            },
        }

        obj = self.schema.load(data)

        assert len(obj.services) == 3
        assert obj.services['value', 'inet'].command == 'command1'
        assert obj.services['value', 'pass'].command == 'command2'
        assert obj.services['value', 'unix'].command == 'command3'
