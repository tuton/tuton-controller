# SPDX-License-Identifier: GPL-3.0-or-later

from collections import OrderedDict
from marshmallow import Schema, fields, pre_dump, post_dump, post_load, EXCLUDE

from ..meta import TypeMeta, v1_ObjectMetaSchema, v1_TypeMetaSchema


class PostfixConfig:
    def __init__(
        self,
        *,
        metadata,
        data=None,
        options=None,
        services=None,
    ):
        self.metadata = metadata
        self.data = data or {}
        self.options = options or {}
        self.services = services or {}

    def update(self, other):
        self.data.update(other.data)
        self.options.update(other.options)
        self.services.update(other.services)


class PostfixConfigOptionSecret:
    def __init__(self, name, secret_name, sub_path):
        self.name = name
        self.secret_name = secret_name
        self.sub_path = sub_path

    @property
    def secret_path(self):
        return f'/run/secrets/tuton.email/{self.secret_name}'

    @property
    def postfixdump(self):
        return f'{self.secret_path}/{self.sub_path}'

    def update(self, other):
        self.secret_name = other.secret_name
        self.sub_path = other.sub_path


class PostfixConfigOptionValue:
    def __init__(self, name, value):
        self.name = name
        self.value = value

    @property
    def postfixdump(self):
        if self.value is True:
            return 'yes'
        if self.value is False:
            return 'no'
        return str(self.value)

    def update(self, other):
        self.value = other.value


class PostfixConfigService:
    def __init__(self, name, type='unix', private=None, unpriv=True, wakeup=None, maxproc=None, command=None, options=None):
        self.name = name
        self.type = type
        self.private = private if private is not None else self.type != 'inet'
        self.unpriv = unpriv
        self.wakeup = None
        self.maxproc = maxproc
        self.command = command or self.name
        self.options = options or {}

    def _postfixdump_boolean(self, i):
        if i is True:
            return 'y'
        elif i is False:
            return 'n'
        return '-'

    def _postfixdump_int(self, i):
        if type(i) is int:
            return str(i)
        return '-'

    def _postfixdump_str(self, i):
        if type(i) is str:
            return i
        return '-'

    @property
    def postfixdump(self):
        e = [
            self.name,
            self.type,
            self._postfixdump_boolean(self.private),
            self._postfixdump_boolean(self.unpriv),
            '-',
            self._postfixdump_str(self.wakeup),
            self._postfixdump_int(self.maxproc),
            self.command,
        ]
        return '  '.join(e)

    def update(self, other):
        self.private = other.private
        self.unpriv = other.unpriv
        self.maxproc = other.maxproc
        self.command = other.command


class v1alpha1_PostfixConfigOptionSecretSchema(Schema):
    secret_name = fields.Str(required=True, data_key='secretName')
    sub_path = fields.Str(required=True, data_key='subPath')


class v1alpha1_PostfixConfigOptionSchema(Schema):
    name = fields.Str(required=True)
    secret = fields.Nested(v1alpha1_PostfixConfigOptionSecretSchema)
    value = fields.Raw()

    @pre_dump
    def dump_obj(self, obj, **kw):
        data = obj.__dict__.copy()
        if isinstance(obj, PostfixConfigOptionSecret):
            data['secret'] = data
        return data

    @post_dump
    def remove_none(self, data, **kw):
        return {i: j for i, j in data.items() if j is not None}

    @post_load
    def load_obj(self, data, **kw):
        name = data['name']
        if 'value' in data:
            return PostfixConfigOptionValue(name, data['value'])
        if 'secret' in data:
            return PostfixConfigOptionSecret(name, **data['secret'])
        raise RuntimeError


class v1alpha1_PostfixConfigServiceSchema(Schema):
    name = fields.Str(required=True)
    type = fields.Str()
    private = fields.Boolean()
    unpriv = fields.Boolean()
    wakeup = fields.Str()
    maxproc = fields.Integer()
    command = fields.Str()
    _options_list = fields.List(fields.Nested(v1alpha1_PostfixConfigOptionSchema), data_key='options', unknown=EXCLUDE)

    @pre_dump
    def dump_obj(self, obj, **kw):
        data = obj.__dict__.copy()
        data['_options_list'] = obj.options.values()
        return data

    @post_dump
    def remove_none(self, data, **kw):
        return {i: j for i, j in data.items() if j is not None}

    @post_load
    def load_obj(self, data, **kw):
        options = OrderedDict((c.name, c) for c in data.pop('_options_list', []))
        return PostfixConfigService(options=options, **data)


class v1alpha1_PostfixConfigSpecSchema(Schema):
    data = fields.Dict(keys=fields.Str(), values=fields.Str())
    _options_list = fields.List(fields.Nested(v1alpha1_PostfixConfigOptionSchema), data_key='options', unknown=EXCLUDE)
    _services_list = fields.List(fields.Nested(v1alpha1_PostfixConfigServiceSchema), data_key='services', unknown=EXCLUDE)


class v1alpha1_PostfixConfigSchema(v1_TypeMetaSchema):
    __model__ = PostfixConfig
    __typemeta__ = TypeMeta('PostfixConfig', 'tuton.email/v1alpha1')

    metadata = fields.Nested(v1_ObjectMetaSchema, required=True, unknown=EXCLUDE)
    spec = fields.Nested(v1alpha1_PostfixConfigSpecSchema, unknown=EXCLUDE)

    @pre_dump
    def dump_obj(self, obj, **kw):
        data = obj.__dict__.copy()
        data['_options_list'] = obj.options.values()
        data['_services_list'] = obj.services.values()
        return {'metadata': obj.metadata, 'spec': data}

    @post_load
    def load_obj(self, data, **kw):
        spec = data.get('spec', {})
        options = OrderedDict((c.name, c) for c in spec.pop('_options_list', []))
        services = OrderedDict(((c.name, c.type), c) for c in spec.pop('_services_list', []))
        return self.__model__(metadata=data['metadata'], options=options, services=services, **spec)
