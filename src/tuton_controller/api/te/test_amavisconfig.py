# SPDX-License-Identifier: GPL-3.0-or-later

import pytest

from tuton_controller.api.te.amavisconfig import (
    AmavisConfigOptionCategories,
    AmavisConfigOptionLiteral,
    AmavisConfigOptionValue,
    v1alpha1_AmavisConfigSchema,
)


class Test_AmavisConfigOptionCategories:
    @pytest.mark.parametrize(
        'out',
        [
            'name => {  }',
        ],
    )
    def test_perldump_ref_assign(self, out):
        v = AmavisConfigOptionCategories('name', {})
        assert v.perldump_ref_assign == out

    @pytest.mark.parametrize(
        'out',
        [
            '%name = (  )',
        ],
    )
    def test_perldump_var_assign(self, out):
        v = AmavisConfigOptionCategories('name', {})
        assert v.perldump_var_assign == out


class Test_AmavisConfigOptionCategoriesLiteral:
    @pytest.mark.parametrize(
        'category,literal,out',
        [
            ('CC_BANNED', 'D_PASS', 'name => { &CC_BANNED => D_PASS }'),
        ],
    )
    def test_perldump_ref_assign(self, category, literal, out):
        v = AmavisConfigOptionCategories('name', {category: AmavisConfigOptionLiteral(category, literal)})
        assert v.perldump_ref_assign == out

    @pytest.mark.parametrize(
        'category,literal,out',
        [
            ('CC_BANNED', 'D_PASS', '%name = ( &CC_BANNED => D_PASS )'),
        ],
    )
    def test_perldump_var_assign(self, category, literal, out):
        v = AmavisConfigOptionCategories('name', {category: AmavisConfigOptionLiteral(category, literal)})
        assert v.perldump_var_assign == out


class Test_AmavisConfigOptionCategoriesValue:
    @pytest.mark.parametrize(
        'category,value,out',
        [
            ('CC_BANNED', True, 'name => { &CC_BANNED => 1 }'),
            ('CC_BANNED', False, 'name => { &CC_BANNED => 0 }'),
            ('CC_BANNED', 'test', "name => { &CC_BANNED => 'test' }"),
            ('CC_BANNED', 23, 'name => { &CC_BANNED => 23 }'),
        ],
    )
    def test_perldump_ref_assign(self, category, value, out):
        v = AmavisConfigOptionCategories('name', {category: AmavisConfigOptionValue(category, value)})
        assert v.perldump_ref_assign == out

    @pytest.mark.parametrize(
        'category,value,out',
        [
            ('CC_BANNED', True, '%name = ( &CC_BANNED => 1 )'),
            ('CC_BANNED', False, '%name = ( &CC_BANNED => 0 )'),
            ('CC_BANNED', 'test', "%name = ( &CC_BANNED => 'test' )"),
            ('CC_BANNED', 23, '%name = ( &CC_BANNED => 23 )'),
        ],
    )
    def test_perldump_var_assign(self, category, value, out):
        v = AmavisConfigOptionCategories('name', {category: AmavisConfigOptionValue(category, value)})
        assert v.perldump_var_assign == out


class Test_AmavisConfigOptionLiteral:
    @pytest.mark.parametrize(
        'literal,out',
        [
            ('D_PASS', 'name => D_PASS'),
            ([1], 'name => [1]'),
            (["'test', []"], "name => ['test', []]"),
        ],
    )
    def test_perldump_ref_assign(self, literal, out):
        v = AmavisConfigOptionLiteral('name', literal)
        assert v.perldump_ref_assign == out

    @pytest.mark.parametrize(
        'literal,out',
        [
            ('D_PASS', '$name = D_PASS'),
            ([1], '@name = (1)'),
            (["'test', []"], "@name = ('test', [])"),
        ],
    )
    def test_perldump_var_assign(self, literal, out):
        v = AmavisConfigOptionLiteral('name', literal)
        assert v.perldump_var_assign == out


class Test_AmavisConfigOptionValue:
    @pytest.mark.parametrize(
        'value,out',
        [
            (True, 'name => 1'),
            (False, 'name => 0'),
            ('test', "name => 'test'"),
            (23, 'name => 23'),
            (object, 'name => undef'),
            ([], 'name => []'),
            ([1, 2, 3, 'test', object, [], {}], "name => [1,2,3,'test',undef,[],{}]"),
            ({'1': 2, '2': 'test', '3': [], '4': {}}, "name => {'1'=>2,'2'=>'test','3'=>[],'4'=>{}}"),
        ],
    )
    def test_perldump_ref_assign(self, value, out):
        v = AmavisConfigOptionValue('name', value)
        assert v.perldump_ref_assign == out

    @pytest.mark.parametrize(
        'value,out',
        [
            (True, '$name = 1'),
            (False, '$name = 0'),
            ('test', "$name = 'test'"),
            (23, '$name = 23'),
            (object, '$name = undef'),
            ([], '@name = ()'),
            ([1, 2, 3, 'test', object, [], {}], "@name = (1,2,3,'test',undef,[],{})"),
            ({'1': 2, '2': 'test', '3': [], '4': {}}, "%name = ('1'=>2,'2'=>'test','3'=>[],'4'=>{})"),
        ],
    )
    def test_perldump_var_assign(self, value, out):
        v = AmavisConfigOptionValue('name', value)
        assert v.perldump_var_assign == out


class Test_v1alpha1_AmavisConfigSchema:
    schema = v1alpha1_AmavisConfigSchema()

    def test(self):
        data = {
            'apiVersion': 'tuton.email/v1alpha1',
            'kind': 'AmavisConfig',
            'metadata': {
                'name': 'name',
                'labels': {},
            },
            'spec': {
                'policybank': {
                    'name': 'policybank',
                    'selector': {},
                },
                'options': [
                    {
                        'name': 'category',
                        'categories': [
                            {
                                'name': 'literal',
                                'literal': 'literal',
                            },
                            {
                                'name': 'value',
                                'value': 'value',
                            },
                        ],
                    },
                    {
                        'name': 'literal',
                        'literal': 'literal',
                    },
                    {
                        'name': 'value',
                        'value': 'value',
                    },
                ],
                'spamassassinConfig': [
                    'value',
                ],
            },
        }

        obj = self.schema.load(data)

        assert len(obj.options) == 3
        assert obj.options['category'].categories['literal'].literal == 'literal'
        assert obj.options['category'].categories['value'].value == 'value'
        assert obj.options['literal'].literal == 'literal'
        assert obj.options['value'].value == 'value'
        assert obj.spamassassin_config == ['value']

        assert data == self.schema.dump(obj)
