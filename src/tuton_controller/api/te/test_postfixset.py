# SPDX-License-Identifier: GPL-3.0-or-later

from tuton_controller.api.te.postfixset import v1alpha1_PostfixSetSchema


class Test_v1alpha1_PostfixSetSchema:
    schema = v1alpha1_PostfixSetSchema()

    def test(self):
        data = {
            'apiVersion': 'tuton.email/v1alpha1',
            'kind': 'PostfixSet',
            'metadata': {
                'name': 'name',
                'labels': {},
            },
            'spec': {
                'containers': [
                    {
                        'name': 'test2',
                        'configSelector': {},
                        'configVersion': None,
                        'image': 'image',
                        'imagePullPolicy': 'policy',
                    },
                    {
                        'name': 'test1',
                        'configSelector': {},
                        'configVersion': None,
                        'image': 'image',
                        'imagePullPolicy': 'policy',
                    },
                ],
                'metadata': {
                    'labels': {},
                },
                'replicas': 1,
                'volumeClaimTemplates': [
                    {
                        'metadata': {
                            'name': 'test',
                            'labels': {},
                        },
                        'spec': {},
                    },
                ],
            },
        }

        obj = self.schema.load(data)

        assert len(obj.containers) == 2
        assert obj.containers['test1'].image == 'image'
        assert obj.containers['test2'].image == 'image'
        assert len(obj.volume_claim_templates) == 1
        assert obj.volume_claim_templates['test'].spec == {}

        assert data == self.schema.dump(obj)
