# SPDX-License-Identifier: GPL-3.0-or-later

from collections import OrderedDict
from marshmallow import Schema, fields, pre_dump, post_load, EXCLUDE

from ..meta import TypeMeta, v1_ObjectMetaSchema, v1_TypeMetaSchema
from .container import v1alpha1_ContainerSchema


class AmavisSet:
    def __init__(
        self,
        *,
        metadata,
        containers=None,
        replicas=None,
        template_metadata=None
    ):
        self.metadata = metadata
        self.containers = containers or {}
        self.replicas = replicas
        self.template_metadata = template_metadata or AmavisSetSpecMeta()


class AmavisSetSpecMeta:
    def __init__(
        self,
        *,
        labels=None,
    ):
        self.labels = labels or {}


class v1alpha1_AmavisSetSpecMetaSchema(Schema):
    labels = fields.Dict(keys=fields.Str(), values=fields.Str())

    @post_load
    def load_obj(self, data, **kw):
        return AmavisSetSpecMeta(**data)


class v1alpha1_AmavisSetSpecSchema(Schema):
    _containers_list = fields.List(fields.Nested(v1alpha1_ContainerSchema), data_key='containers', unknown=EXCLUDE)
    replicas = fields.Int(missing=1)
    template_metadata = fields.Nested(v1alpha1_AmavisSetSpecMetaSchema, data_key='metadata')


class v1alpha1_AmavisSetSchema(v1_TypeMetaSchema):
    __model__ = AmavisSet
    __typemeta__ = TypeMeta('AmavisSet', 'tuton.email/v1alpha1')

    metadata = fields.Nested(v1_ObjectMetaSchema, required=True, unknown=EXCLUDE)
    spec = fields.Nested(v1alpha1_AmavisSetSpecSchema, unknown=EXCLUDE)

    @pre_dump
    def dump_obj(self, obj, **kw):
        data = obj.__dict__.copy()
        data['_containers_list'] = obj.containers.values()
        return {'metadata': obj.metadata, 'spec': data}

    @post_load
    def load_obj(self, data, **kw):
        spec = data['spec']
        containers = OrderedDict((c.name, c) for c in spec.pop('_containers_list', []))
        return self.__model__(metadata=data['metadata'], containers=containers, **spec)
