# SPDX-License-Identifier: GPL-3.0-or-later

from tuton_controller.api.te.amavisset import v1alpha1_AmavisSetSchema


class Test_v1alpha1_AmavisSetSchema:
    schema = v1alpha1_AmavisSetSchema()

    def test(self):
        data = {
            'apiVersion': 'tuton.email/v1alpha1',
            'kind': 'AmavisSet',
            'metadata': {
                'name': 'name',
                'labels': {},
            },
            'spec': {
                'containers': [
                    {
                        'name': 'test2',
                        'configSelector': {},
                        'configVersion': None,
                        'image': 'image',
                        'imagePullPolicy': 'policy',
                    },
                    {
                        'name': 'test1',
                        'configSelector': {},
                        'configVersion': None,
                        'image': 'image',
                        'imagePullPolicy': 'policy',
                    },
                ],
                'metadata': {
                    'labels': {},
                },
                'replicas': 1,
            },
        }

        obj = self.schema.load(data)

        assert len(obj.containers) == 2
        assert obj.containers['test1'].image == 'image'
        assert obj.containers['test2'].image == 'image'

        assert data == self.schema.dump(obj)
