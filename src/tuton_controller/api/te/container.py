# SPDX-License-Identifier: GPL-3.0-or-later

from marshmallow import Schema, fields, post_load


class Container:
    def __init__(
        self,
        name,
        config_selector=None,
        config_version=None,
        image=None,
        image_pull_policy=None,
    ):
        self.name = name
        self.image = image
        self.image_pull_policy = image_pull_policy
        self.config_selector = config_selector or {}
        self.config_version = config_version


class v1alpha1_ContainerSchema(Schema):
    config_selector = fields.Dict(data_key='configSelector', allow_none=True)
    config_version = fields.Str(data_key='configVersion', allow_none=True)
    image = fields.Str(required=True)
    image_pull_policy = fields.Str(data_key='imagePullPolicy', missing='Always')
    name = fields.Str(required=True)

    @post_load
    def load_obj(self, data, **kw):
        return Container(**data)
