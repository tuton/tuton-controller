# SPDX-License-Identifier: GPL-3.0-or-later

from collections import OrderedDict
from marshmallow import Schema, fields, pre_dump, post_load, EXCLUDE

from ..meta import TypeMeta, v1_ObjectMetaSchema, v1_TypeMetaSchema
from .container import v1alpha1_ContainerSchema


class PostfixSet:
    def __init__(
        self,
        *,
        metadata,
        containers=None,
        replicas=None,
        template_metadata=None,
        volume_claim_templates=None,
    ):
        self.metadata = metadata
        self.containers = containers or {}
        self.replicas = replicas
        self.template_metadata = template_metadata or PostfixSetSpecMeta()
        self.volume_claim_templates = volume_claim_templates or {}


class PostfixSetSpecMeta:
    def __init__(
        self,
        *,
        labels=None,
    ):
        self.labels = labels or {}


class PostfixSetSpecVolumeclaimtemplate:
    def __init__(
        self,
        *,
        metadata,
        spec=None,
    ):
        self.metadata = metadata
        self.spec = spec


class v1alpha1_PostfixSetSpecMetaSchema(Schema):
    labels = fields.Dict(keys=fields.Str(), values=fields.Str())

    @post_load
    def load_obj(self, data, **kw):
        return PostfixSetSpecMeta(**data)


class v1alpha1_PostfixSetSpecVolumeclaimtemplateSchema(Schema):
    metadata = fields.Nested(v1_ObjectMetaSchema, required=True, unknown=EXCLUDE)
    spec = fields.Dict(keys=fields.Str())

    @post_load
    def load_obj(self, data, **kw):
        return PostfixSetSpecVolumeclaimtemplate(**data)


class v1alpha1_PostfixSetSpecSchema(Schema):
    _containers_list = fields.List(fields.Nested(v1alpha1_ContainerSchema), data_key='containers', unknown=EXCLUDE)
    replicas = fields.Int(missing=1)
    template_metadata = fields.Nested(v1alpha1_PostfixSetSpecMetaSchema, data_key='metadata')
    _volume_claim_templates_list = fields.List(fields.Nested(v1alpha1_PostfixSetSpecVolumeclaimtemplateSchema), data_key='volumeClaimTemplates', unknown=EXCLUDE)


class v1alpha1_PostfixSetSchema(v1_TypeMetaSchema):
    __model__ = PostfixSet
    __typemeta__ = TypeMeta('PostfixSet', 'tuton.email/v1alpha1')

    metadata = fields.Nested(v1_ObjectMetaSchema, required=True, unknown=EXCLUDE)
    spec = fields.Nested(v1alpha1_PostfixSetSpecSchema, unknown=EXCLUDE)

    @pre_dump
    def dump_obj(self, obj, **kw):
        data = obj.__dict__.copy()
        data['_containers_list'] = obj.containers.values()
        data['_volume_claim_templates_list'] = obj.volume_claim_templates.values()
        return {'metadata': obj.metadata, 'spec': data}

    @post_load
    def load_obj(self, data, **kw):
        spec = data['spec']
        containers = OrderedDict((c.name, c) for c in spec.pop('_containers_list', []))
        volume_claim_templates = OrderedDict((c.metadata.name, c) for c in spec.pop('_volume_claim_templates_list', []))
        return self.__model__(
            metadata=data['metadata'],
            containers=containers,
            volume_claim_templates=volume_claim_templates,
            **spec
        )
