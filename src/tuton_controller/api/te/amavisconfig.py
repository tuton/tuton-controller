# SPDX-License-Identifier: GPL-3.0-or-later

from collections import OrderedDict
from marshmallow import Schema, fields, pre_dump, post_load, EXCLUDE

from ..meta import TypeMeta, v1_ObjectMetaSchema, v1_TypeMetaSchema


class AmavisConfig:
    def __init__(
        self,
        *,
        metadata,
        options=None,
        policybank=None,
        spamassassin_config=None,
    ):
        self.metadata = metadata
        self.options = options or {}
        self.policybank = policybank or AmavisConfigPolicybank(None)
        self.spamassassin_config = spamassassin_config or []

    @property
    def policybank_name(self):
        if self.policybank:
            return self.policybank.name

    def update(self, other):
        self.options.update(other.options)
        self.policybank.update(other.policybank)
        self.spamassassin_config.extend(other.spamassassin_config)


class AmavisConfigOptionBase:
    @classmethod
    def perldump_type(cls, value):
        raise NotImplementedError

    @classmethod
    def perldump_type_array(cls, value):
        return ','.join(cls.perldump_ref_simple(i) for i in value)

    @classmethod
    def perldump_type_hash(cls, value):
        return ','.join(f'{cls.perldump_type(i)}=>{cls.perldump_ref_simple(j)}'
                        for i, j in sorted(value.items()))

    @property
    def perldump_ref(self):
        raise NotImplementedError

    @property
    def perldump_ref_assign(self):
        return f'{self.name} => {self.perldump_ref}'

    @classmethod
    def perldump_ref_simple(cls, value):
        if isinstance(value, list):
            return '[' + cls.perldump_type_array(value) + ']'
        if isinstance(value, dict):
            return '{' + cls.perldump_type_hash(value) + '}'
        return cls.perldump_type(value)

    @property
    def perldump_var(self):
        raise NotImplementedError

    @classmethod
    def perldump_var_simple(cls, value):
        if isinstance(value, list):
            return '(' + cls.perldump_type_array(value) + ')'
        if isinstance(value, dict):
            return '(' + cls.perldump_type_hash(value) + ')'
        return cls.perldump_type(value)

    @property
    def perldump_var_assign(self):
        return f'{self.perldump_var_type}{self.name} = {self.perldump_var}'

    @property
    def perldump_var_type(self):
        raise NotImplementedError


class AmavisConfigOptionCategories(AmavisConfigOptionBase):
    def __init__(self, name, categories):
        self.name = name
        self.categories = categories

    @property
    def perldump_ref(self):
        o = []
        for i in self.categories.values():
            o.append(f'&{i.name} => {i.perldump_ref}')
        return '{{ {} }}'.format(', '.join(o))

    @property
    def perldump_var(self):
        o = []
        for i in self.categories.values():
            o.append(f'&{i.name} => {i.perldump_ref}')
        return '( {} )'.format(', '.join(o))

    @property
    def perldump_var_type(self):
        return '%'


class AmavisConfigOptionLiteral(AmavisConfigOptionBase):
    def __init__(self, name, literal):
        self.name = name
        self.literal = literal

    @classmethod
    def perldump_type(cls, value):
        return f'{value}'

    @property
    def perldump_ref(self):
        return self.perldump_ref_simple(self.literal)

    @property
    def perldump_var(self):
        return self.perldump_var_simple(self.literal)

    @property
    def perldump_var_type(self):
        if isinstance(self.literal, list):
            return '@'
        if isinstance(self.literal, dict):
            return '%'
        return '$'

    def update(self, other):
        self.literal = other.literal


class AmavisConfigOptionValue(AmavisConfigOptionBase):
    def __init__(self, name, value):
        self.name = name
        self.value = value

    @classmethod
    def perldump_type(cls, value):
        if value is True:
            return '1'
        if value is False:
            return '0'
        if isinstance(value, str):
            return f"'{value}'"
        if isinstance(value, int):
            return f'{value}'
        return 'undef'

    @property
    def perldump_ref(self):
        return self.perldump_ref_simple(self.value)

    @property
    def perldump_var(self):
        return self.perldump_var_simple(self.value)

    @property
    def perldump_var_type(self):
        if isinstance(self.value, list):
            return '@'
        if isinstance(self.value, dict):
            return '%'
        return '$'

    def update(self, other):
        self.value = other.value


class AmavisConfigPolicybank:
    def __init__(self, name, selector=None):
        self.name = name
        self.selector = selector or {}

    def update(self, other):
        self.name = other.name
        self.selector.update(other.selector)


class v1alpha1_AmavisConfigCategorySchema(Schema):
    name = fields.Str(required=True)
    literal = fields.Str()
    value = fields.Raw()

    @post_load
    def load_obj(self, data, **kw):
        name = data['name']
        if 'value' in data:
            return AmavisConfigOptionValue(name, data['value'])
        if 'literal' in data:
            return AmavisConfigOptionLiteral(name, data['literal'])


class v1alpha1_AmavisConfigOptionSchema(Schema):
    name = fields.Str(required=True)
    _categories_list = fields.List(fields.Nested(v1alpha1_AmavisConfigCategorySchema), data_key='categories', unknown=EXCLUDE)
    literal = fields.Raw()
    value = fields.Raw()

    @pre_dump
    def dump_obj(self, obj, **kw):
        data = obj.__dict__.copy()
        if isinstance(obj, AmavisConfigOptionCategories):
            data['_categories_list'] = obj.categories.values()
        return data

    @post_load
    def load_obj(self, data, **kw):
        name = data['name']
        if 'value' in data:
            return AmavisConfigOptionValue(name, data['value'])
        if 'literal' in data:
            return AmavisConfigOptionLiteral(name, data['literal'])
        if '_categories_list' in data:
            categories = OrderedDict((c.name, c) for c in data['_categories_list'])
            return AmavisConfigOptionCategories(name, categories)


class v1alpha1_AmavisConfigPolicybankSchema(Schema):
    name = fields.Str(required=True)
    selector = fields.Dict()

    @post_load
    def load_obj(self, data, **kw):
        return AmavisConfigPolicybank(**data)


class v1alpha1_AmavisConfigSpecSchema(Schema):
    _options_list = fields.List(fields.Nested(v1alpha1_AmavisConfigOptionSchema), data_key='options', unknown=EXCLUDE)
    policybank = fields.Nested(v1alpha1_AmavisConfigPolicybankSchema, unknown=EXCLUDE)
    spamassassin_config = fields.List(fields.Str(), data_key='spamassassinConfig')


class v1alpha1_AmavisConfigSchema(v1_TypeMetaSchema):
    __model__ = AmavisConfig
    __typemeta__ = TypeMeta('AmavisConfig', 'tuton.email/v1alpha1')

    metadata = fields.Nested(v1_ObjectMetaSchema, required=True, unknown=EXCLUDE)
    spec = fields.Nested(v1alpha1_AmavisConfigSpecSchema, unknown=EXCLUDE)

    @pre_dump
    def dump_obj(self, obj, **kw):
        data = obj.__dict__.copy()
        data['_options_list'] = obj.options.values()
        return {'metadata': obj.metadata, 'spec': data}

    @post_load
    def load_obj(self, data, **kw):
        spec = data.get('spec', {})
        options = OrderedDict((c.name, c) for c in spec.pop('_options_list', []))
        return self.__model__(metadata=data['metadata'], options=options, **spec)
