# SPDX-License-Identifier: GPL-3.0-or-later

from collections import namedtuple
from marshmallow import Schema, fields, post_dump, post_load, ValidationError, validates


TypeMeta = namedtuple('TypeMeta', ['kind', 'api_version'])


class v1_TypeMetaSchema(Schema):
    __model__ = TypeMeta
    __typemeta__ = None

    api_version = fields.Str(required=True, data_key='apiVersion')
    kind = fields.Str(required=True)

    @post_dump
    def dump_typemeta(self, data, **kw):
        if self.__typemeta__:
            data['apiVersion'] = self.__typemeta__.api_version
            data['kind'] = self.__typemeta__.kind
        return data

    @validates('api_version')
    def validate_api_version(self, data):
        if self.__typemeta__ and self.__typemeta__.api_version != data:
            raise ValidationError('Input is of wrong api version')

    @validates('kind')
    def validate_kind(self, data):
        if self.__typemeta__ and self.__typemeta__.kind != data:
            raise ValidationError('Input is of wrong kind')


class ObjectMeta:
    def __init__(self, name, labels=None, uid=None):
        self.name = name
        self.labels = labels or {}
        self.uid = uid

    def copy(self):
        return self.__class__(
            name=self.name,
            labels=self.labels.copy(),
        )


class v1_ObjectMetaSchema(Schema):
    name = fields.String(required=True)
    labels = fields.Dict(keys=fields.Str(), values=fields.Str())
    uid = fields.Str(allow_none=True)

    @post_dump
    def remove_none(self, data, **kw):
        return {i: j for i, j in data.items() if j is not None}

    @post_load
    def make_object(self, data, **kw):
        return ObjectMeta(**data)
