# SPDX-License-Identifier: GPL-3.0-or-later


from ..api.te.postfixconfig import PostfixConfigOptionSecret
from ..base.deployment import Deployment as BaseDeployment


class StatefulSet(BaseDeployment):
    kind = 'StatefulSet'

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)

        self.secrets = {
            j.secret_name: j
            for i, j in sorted(self.config.merge_config().options.items())
            if isinstance(j, PostfixConfigOptionSecret)
        }

    @property
    def spec(self):
        return {
            'replicas': self.spec_replicas,
            'selector': self.spec_selector,
            'template': self.spec_template,
            'volumeClaimTemplates': self.spec_volume_claim_templates,
        }

    @property
    def spec_template_spec(self):
        return {
            'containers': self.spec_template_spec_containers,
            # TODO: Needs to match 'postfix' group of actually used image
            'securityContext': {
                'fsGroup': 999,
            },
            'volumes': self.spec_template_spec_volumes,
        }

    @property
    def spec_template_spec_containers(self):
        ret = []
        if 'postfix' in self.parent.containers:
            ret.append(self.spec_template_spec_containers_postfix(self.parent.containers['postfix']))
        if 'rsyslog' in self.parent.containers:
            ret.append(self.spec_template_spec_containers_rsyslog(self.parent.containers['rsyslog']))
        return ret

    def spec_template_spec_containers_postfix(self, c):
        return {
            'name': 'postfix',
            'image': c.image,
            'imagePullPolicy': c.image_pull_policy,
            'args': ['-c', '/etc/postfix-local'],
            'securityContext': {
                'allowPrivilegeEscalation': False,
                'readOnlyRootFilesystem': True,
            },
            'volumeMounts': [
                {
                    'name': 'postfix-config',
                    'mountPath': '/etc/postfix-local',
                },
                {
                    'name': 'data',
                    'mountPath': '/var/lib/postfix',
                },
                {
                    'name': 'queue',
                    'mountPath': '/var/spool/postfix',
                },
                {
                    'name': 'rsyslog-run',
                    'mountPath': '/run/rsyslog',
                },
            ] + [
                {
                    'name': f'secret-{i.secret_name}',
                    'mountPath': i.secret_path,
                }
                for i in self.secrets.values()
            ]
        }

    def spec_template_spec_containers_rsyslog(self, c):
        return {
            'name': 'rsyslog',
            'image': c.image,
            'imagePullPolicy': c.image_pull_policy,
            'securityContext': {
                'allowPrivilegeEscalation': False,
                'readOnlyRootFilesystem': True,
            },
            'volumeMounts': [
                {
                    'name': 'rsyslog-run',
                    'mountPath': '/run/rsyslog',
                },
            ],
        }

    @property
    def spec_template_spec_volumes(self):
        return [
            {
                'name': 'postfix-config',
                'configMap': {'name': self.parent.metadata.name},
            },
            {
                'name': 'rsyslog-run',
                'emptyDir': {'medium': 'Memory'},
            },
        ] + [
            {
                'name': f'secret-{i}',
                'secret': {
                    'secretName': i,
                },
            }
            for i in self.secrets.keys()
        ]

    def spec_volume_claim_templates_default(self, name, size):
        return {
            'metadata': {
                'name': name,
            },
            'spec': {
                'accessModes': [
                    'ReadWriteOnce',
                ],
                'resources': {
                    'requests': {
                        'storage': size,
                    },
                },
            },
        }

    def spec_volume_claim_templates_import(self, t):
        return {
            'metadata': {
                'name': t.metadata.name,
            },
            'spec': t.spec,
        }

    @property
    def spec_volume_claim_templates(self):
        ret = []
        if 'data' in self.parent.volume_claim_templates:
            ret.append(self.spec_volume_claim_templates_import(self.parent.volume_claim_templates['data']))
        else:
            ret.append(self.spec_volume_claim_templates_default('data', '128Mi'))
        if 'queue' in self.parent.volume_claim_templates:
            ret.append(self.spec_volume_claim_templates_import(self.parent.volume_claim_templates['queue']))
        else:
            ret.append(self.spec_volume_claim_templates_default('queue', '4Gi'))
        return ret
