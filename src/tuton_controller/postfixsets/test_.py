from . import customize, sync
from .. import app


class TestCustomize:
    basic_v1alpha1_PostfixSet = {
        'apiVersion': 'tuton.email/v1alpha1',
        'kind': 'PostfixSet',
        'metadata': {
            'name': 'test',
        },
        'spec': {
        },
    }

    def test_basic_v1alpha1(self):
        data = {
            'parent': self.basic_v1alpha1_PostfixSet,
        }
        with app.test_request_context('/postfixset/', json=data):
            customize()


class TestSync:
    basic_v1alpha1_PostfixSet = {
        'apiVersion': 'tuton.email/v1alpha1',
        'kind': 'PostfixSet',
        'metadata': {
            'name': 'test',
        },
        'spec': {
        },
    }

    basic_v1alpha1_PostfixConfig = {
        'apiVersion': 'tuton.email/v1alpha1',
        'kind': 'PostfixConfig',
        'metadata': {
            'name': 'test',
        },
    }

    def test_basic_v1alpha1(self):
        data = {
            'parent': self.basic_v1alpha1_PostfixSet,
            'children': {},
            'related': {},
        }
        with app.test_request_context('/postfixset/', json=data):
            sync()

    def test_related_v1alpha1(self):
        data = {
            'parent': self.basic_v1alpha1_PostfixSet,
            'children': {},
            'related': {
                'PostfixConfig.tuton.email/v1alpha1': {
                    'test': self.basic_v1alpha1_PostfixConfig,
                },
            },
        }
        with app.test_request_context('/postfixset/', json=data):
            sync()
