# SPDX-License-Identifier: GPL-3.0-or-later

from .relatedresources import RelatedResources

from ..api.te.container import Container
from ..api.te.postfixset import PostfixSet


def test_container_existing():
    r = RelatedResources(
        parent=PostfixSet(
            metadata=None,
            containers={
                'postfix': Container(
                    name='postfix',
                    config_selector={'key': 'value'},
                ),
            },
        ),
    )

    o = r()
    assert len(o) == 1
    assert o[0] == {
        'apiVersion': 'tuton.email/v1alpha1',
        'resource': 'postfixconfigs',
        'labelSelector': {'key': 'value'},
    }


def test_container_empty():
    r = RelatedResources(
        parent=PostfixSet(
            metadata=None,
            containers={
                'postfix': Container(
                    name='postfix',
                ),
            },
        ),
    )

    o = r()
    assert len(o) == 1
    assert o[0] == {
        'apiVersion': 'tuton.email/v1alpha1',
        'resource': 'postfixconfigs',
        'labelSelector': {},
    }


def test_container_nonexisting():
    r = RelatedResources(
        parent=PostfixSet(
            metadata=None,
            containers={
                'unknown': Container(
                    name='unknown',
                    config_selector={'key': 'value'},
                ),
            },
        ),
    )

    assert r() == []
