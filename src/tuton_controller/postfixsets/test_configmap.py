# SPDX-License-Identifier: GPL-3.0-or-later

from .configmap import ConfigMap

from .. import app
from ..api.meta import ObjectMeta
from ..api.te.postfixset import PostfixSet


test_metadata = ObjectMeta(name='test', uid='test')


def test_empty():
    postfixset = PostfixSet(metadata=test_metadata)

    configmap = ConfigMap(postfixset, [], load_base_config=False)
    config = configmap.merge_config()

    assert config.data == {}
    assert config.options == {}
    assert config.services == {}

    with app.test_request_context('/postfixset/'):
        rendered_configmap = configmap()

    assert rendered_configmap == {
        'apiVersion': 'v1',
        'kind': 'ConfigMap',
        'metadata': {
            'name': 'test',
        },
        'data': {
            'main.cf': '',
            'master.cf': '',
        },
    }


def test_base():
    postfixset = PostfixSet(metadata=test_metadata)

    configmap = ConfigMap(postfixset, [])
    config = configmap.merge_config()

    assert config.data == {}
    assert config.options == {}
    assert len(config.services) == 17

    with app.test_request_context('/postfixset/'):
        configmap()
