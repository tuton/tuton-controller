# SPDX-License-Identifier: GPL-3.0-or-later


class RelatedResources:
    def __init__(self, parent):
        self.parent = parent

    def __call__(self):
        return self.related_postfix

    @property
    def related_postfix(self):
        container = self.parent.containers.get('postfix')
        if not container:
            return []

        return [
            {
                'apiVersion': 'tuton.email/v1alpha1',
                'resource': 'postfixconfigs',
                'labelSelector': container.config_selector,
            },
        ]
