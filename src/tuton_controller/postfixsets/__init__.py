# SPDX-License-Identifier: GPL-3.0-or-later

from flask import Blueprint, jsonify, request
from marshmallow import EXCLUDE

from ..api.te.postfixconfig import v1alpha1_PostfixConfigSchema
from ..api.te.postfixset import v1alpha1_PostfixSetSchema
from .configmap import ConfigMap
from .relatedresources import RelatedResources
from .service import Service
from .statefulset import StatefulSet
from .status import Status


controller = Blueprint('postfixsets', __name__, template_folder='templates', url_prefix='/postfixsets')


@controller.route('/customize', methods=('POST', ))
def customize():
    data = request.get_json()
    parent = v1alpha1_PostfixSetSchema(unknown=EXCLUDE).load(data['parent'])
    related = RelatedResources(parent)
    return jsonify({'relatedResources': related()})


@controller.route('/sync', methods=('POST', ))
def sync():
    data = request.get_json()
    parent = v1alpha1_PostfixSetSchema(unknown=EXCLUDE).load(data['parent'])
    children = data['children']
    related = data['related']

    config = []
    for n in ('PostfixConfig.tuton.email/v1alpha1', ):
        config.extend(v1alpha1_PostfixConfigSchema().load(j) for i, j in sorted(related.get(n, {}).items()))

    status = Status(parent, children)
    configmap = ConfigMap(parent, config)
    statefulset = StatefulSet(parent, configmap)
    service = Service(parent)
    return jsonify({
        'status': status(),
        'children': [configmap(), statefulset(), service()],
    })
