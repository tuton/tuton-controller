# SPDX-License-Identifier: GPL-3.0-or-later

import hashlib

from flask import render_template

from ..api.te.postfixconfig import PostfixConfig, v1alpha1_PostfixConfigSchema
from ..base.configmap import ConfigMap as BaseConfigMap


class ConfigMap(BaseConfigMap):
    schema = v1alpha1_PostfixConfigSchema()

    base_config = schema.load({
        'apiVersion': 'tuton.email/v1alpha1',
        'kind': 'PostfixConfig',
        'metadata': {
            'name': 'base',
        },
        'spec': {
            'services': [
                # cleanup   unix  n       -       -       -       0       cleanup
                {
                    'name': 'cleanup',
                    'private': False,
                    'maxproc': 0,
                },
                # qmgr      unix  n       -       -       300     1       qmgr
                {
                    'name': 'qmgr',
                    'private': False,
                    'wakeup': '300',
                    'maxproc': 1,
                },
                # tlsmgr    unix  -       -       -       1000?   1       tlsmgr
                {
                    'name': 'tlsmgr',
                    'wakeup': '1000?',
                    'maxproc': 1,
                },
                # rewrite   unix  -       -       -       -       -       trivial-rewrite
                {
                    'name': 'rewrite',
                    'command': 'trivial-rewrite',
                },
                # bounce    unix  -       -       -       -       0       bounce
                {
                    'name': 'bounce',
                    'maxproc': 0,
                    'command': 'bounce',
                },
                # defer     unix  -       -       -       -       0       bounce
                {
                    'name': 'defer',
                    'maxproc': 0,
                    'command': 'bounce',
                },
                # trace     unix  -       -       -       -       0       bounce
                {
                    'name': 'trace',
                    'maxproc': 0,
                    'command': 'bounce',
                },
                # verify    unix  -       -       -       -       1       verify
                {
                    'name': 'verify',
                    'maxproc': 1,
                },
                # flush     unix  n       -       -       1000?   0       flush
                {
                    'name': 'flush',
                    'private': False,
                    'wakeup': '1000?',
                    'maxproc': 1,
                },
                # proxymap  unix  -       -       n       -       -       proxymap
                {
                    'name': 'proxymap',
                },
                # proxywrite unix -       -       n       -       1       proxymap
                {
                    'name': 'proxywrite',
                    'maxproc': 1,
                    'command': 'proxymap',
                },
                # showq     unix  n       -       -       -       -       showq
                {
                    'name': 'showq',
                    'private': False,
                },
                # error     unix  -       -       -       -       -       error
                {
                    'name': 'error',
                },
                # retry     unix  -       -       -       -       -       error
                {
                    'name': 'retry',
                    'command': 'error',
                },
                # discard   unix  -       -       -       -       -       discard
                {
                    'name': 'discard',
                },
                # anvil     unix  -       -       -       -       1       anvil
                {
                    'name': 'anvil',
                    'maxproc': 1,
                },
                # scache    unix  -       -       -       -       1       scache
                {
                    'name': 'scache',
                    'maxproc': 1,
                },
            ],
        },
    })

    def __init__(self, parent, config, load_base_config=True):
        super().__init__(parent)
        self.config = config
        if load_base_config:
            self.config.append(self.base_config)

    @property
    def checksum(self):
        m = hashlib.sha256()
        for i, j in sorted(self.data.items()):
            m.update(i.encode('utf-8'))
            m.update(j.encode('utf-8'))
        return m.hexdigest()

    @property
    def data(self):
        config = self.merge_config()
        data = config.data
        data.update({
            'main.cf': render_template('main.cf.j2', config=config),
            'master.cf': render_template('master.cf.j2', config=config),
        })
        return data

    def merge_config(self):
        config = PostfixConfig(metadata=self.parent.metadata)
        for c in self.config:
            config.update(c)
        return config
