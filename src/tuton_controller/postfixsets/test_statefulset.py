# SPDX-License-Identifier: GPL-3.0-or-later

from .statefulset import StatefulSet

from .configmap import ConfigMap
from .. import app
from ..api.meta import ObjectMeta
from ..api.te.container import Container
from ..api.te.postfixconfig import PostfixConfig, PostfixConfigOptionSecret
from ..api.te.postfixset import PostfixSet, PostfixSetSpecMeta


test_metadata = ObjectMeta(name='test', uid='test')


def test_base():
    postfixset = PostfixSet(
        metadata=test_metadata,
        template_metadata=PostfixSetSpecMeta(
            labels={
                'label': 'value',
            },
        ),
        containers={
            'postfix': Container(
                name='postfix',
            ),
            'rsyslog': Container(
                name='rsyslog',
            ),
        },
    )
    configmap = ConfigMap(postfixset, [])

    statefulset = StatefulSet(postfixset, configmap)

    with app.test_request_context('/postfixset/'):
        rendered_statefulset = statefulset()

    assert rendered_statefulset == {
        'apiVersion': 'apps/v1',
        'kind': 'StatefulSet',
        'metadata': {'name': 'test'},
        'spec': {
            'replicas': None,
            'selector': {
                'matchLabels': {'controller-uid': 'test'},
            },
            'template': {
                'metadata': {
                    'name': 'test',
                    'labels': {
                        'controller-uid': 'test',
                        'label': 'value',
                    },
                    'annotations': {'controller.tuton.email/config-checksum': '41bfd6731dc3f182dc8bbb1b891dd433c196b910caae61785e2c77fdfb220718'},
                },
                'spec': {
                    'containers': [
                        {
                            'args': ['-c', '/etc/postfix-local'],
                            'image': None,
                            'imagePullPolicy': None,
                            'name': 'postfix',
                            'securityContext': {
                                'allowPrivilegeEscalation': False,
                                'readOnlyRootFilesystem': True,
                            },
                            'volumeMounts': [
                                {
                                    'mountPath': '/etc/postfix-local',
                                    'name': 'postfix-config',
                                },
                                {
                                    'mountPath': '/var/lib/postfix',
                                    'name': 'data',
                                },
                                {
                                    'mountPath': '/var/spool/postfix',
                                    'name': 'queue',
                                },
                                {
                                    'mountPath': '/run/rsyslog',
                                    'name': 'rsyslog-run',
                                },
                            ],
                        },
                        {
                            'image': None,
                            'imagePullPolicy': None,
                            'name': 'rsyslog',
                            'securityContext': {
                                'allowPrivilegeEscalation': False,
                                'readOnlyRootFilesystem': True,
                            },
                            'volumeMounts': [
                                {
                                    'mountPath': '/run/rsyslog',
                                    'name': 'rsyslog-run',
                                },
                            ],
                        },
                    ],
                    'securityContext': {
                        'fsGroup': 999,
                    },
                    'volumes': [
                        {
                            'name': 'postfix-config',
                            'configMap': {'name': 'test'},
                        },
                        {
                            'name': 'rsyslog-run',
                            'emptyDir': {'medium': 'Memory'}
                        },
                    ],
                },
            },
            'volumeClaimTemplates': [
                {
                    'metadata': {'name': 'data'},
                    'spec': {
                        'accessModes': ['ReadWriteOnce'],
                        'resources': {'requests': {'storage': '128Mi'}},
                    },
                },
                {
                    'metadata': {'name': 'queue'},
                    'spec': {
                        'accessModes': ['ReadWriteOnce'],
                        'resources': {'requests': {'storage': '4Gi'}},
                    },
                },
            ],
        },
    }


def test_secret():
    postfixset = PostfixSet(
        metadata=test_metadata,
        containers={
            'postfix': Container(
                name='postfix',
            ),
        },
    )
    configmap = ConfigMap(
        postfixset,
        [
            PostfixConfig(
                metadata=None,
                options={
                    'test_secret': PostfixConfigOptionSecret('test_secret', 'secret', 'path'),
                },
            ),
        ],
    )

    statefulset = StatefulSet(postfixset, configmap)

    with app.test_request_context('/postfixset/'):
        rendered_statefulset = statefulset()

    assert rendered_statefulset['spec']['template']['spec']['volumes'] == [
        {
            'name': 'postfix-config',
            'configMap': {'name': 'test'},
        },
        {
            'name': 'rsyslog-run',
            'emptyDir': {'medium': 'Memory'}
        },
        {
            'name': 'secret-secret',
            'secret': {
                'secretName': 'secret',
            },
        },
    ]
    assert rendered_statefulset['spec']['template']['spec']['containers'][0]['volumeMounts'] == [
        {
            'mountPath': '/etc/postfix-local',
            'name': 'postfix-config',
        },
        {
            'mountPath': '/var/lib/postfix',
            'name': 'data',
        },
        {
            'mountPath': '/var/spool/postfix',
            'name': 'queue',
        },
        {
            'mountPath': '/run/rsyslog',
            'name': 'rsyslog-run',
        },
        {
            'mountPath': '/run/secrets/tuton.email/secret',
            'name': 'secret-secret',
        },
    ]
