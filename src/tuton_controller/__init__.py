# SPDX-License-Identifier: GPL-3.0-or-later

from flask import Flask


def create_app():
    app = Flask(__name__, instance_relative_config=True)

    @app.route('/')
    def root():
        return 'Tuton controller'

    app.jinja_env.lstrip_blocks = True
    app.jinja_env.trim_blocks = True

    from .amavissets import controller
    app.register_blueprint(controller)
    from .postfixsets import controller
    app.register_blueprint(controller)

    return app


app = create_app()
