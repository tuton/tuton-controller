# SPDX-License-Identifier: GPL-3.0-or-later

import pytest

from tuton_controller import create_app


@pytest.fixture
def app():
    app = create_app()
    return app
